'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var js_files = [
    'node_modules/stickyfilljs/dist/stickyfill.min.js',
    'src/js/*'
];
var css_files = [
    'src/scss/**/*'
];

gulp.task('default', ['css', 'js'], function() {
    // convert sass to clean minified css and include it
    gulp.watch(css_files, ['css']);
    gulp.watch(js_files, ['js'])
});

gulp.task('build',['css', 'js']);

gulp.task('css', function() {
    gulp.src(css_files)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public_html/wp-content/themes/save-so-simply/assets/css/'));
});

gulp.task('js', function() {
    gulp.src(js_files)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public_html/wp-content/themes/save-so-simply/assets/js/'))
});