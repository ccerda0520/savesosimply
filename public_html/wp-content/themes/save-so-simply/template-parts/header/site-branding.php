<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?>
<div class="header">

	<?php if ( has_custom_logo() ) : ?>
		<div class="site-logo"><?php the_custom_logo(); ?></div>
	<?php endif; ?>
	<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentynineteen' ); ?>">
        <div id="header-search-bar">
            <form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
                <label>
                    <span class="screen-reader-text">Main Search</span>
                    <input type="text" placeholder="Search" value name="s">
                </label>
                <button type="submit" class="sss-icon-search search-form-submit"></button>
            </form>
        </div>
        <div class="user-menu">
            <?php if(!is_user_logged_in()) {?>
                <a class="header-login-button" href="/login">Login</a>
            <?php } else { ?>
                <div class="user-menu-icon sss-icon-user"></div>
                <div class="user-menu-links-container">
                    <div class="user-menu-links">
                        <div class="user-menu-link"><a href="/my-account">My Account</a></div>
                        <div class="user-menu-link"><a href="/favorites">My Favorites</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>
		</nav><!-- #site-navigation -->

</div><!-- .site-branding -->