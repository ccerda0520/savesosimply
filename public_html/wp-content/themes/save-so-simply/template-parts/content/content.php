<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="deals-archive-deal">
            <?php
            $title = get_the_title();
            $link = get_the_permalink();
            $featuredImage = get_the_post_thumbnail(get_the_ID(), 'archive-deal-image');
            $content = get_the_content();
            $excerpt = explode(" ", $content);
            $contentLength = count($excerpt);
            $excerpt = implode(" ", array_splice($excerpt, 0, 65));
            $excerpt = $contentLength > 65 ? $excerpt . '...' . " <a href='$link'>Read More</a>" : $excerpt;
            $excerpt = strip_tags($excerpt, '<p><a>');
            $taxonomies = wp_get_object_terms( get_the_ID(), 'deals_category' );
            ?>
            <a class="deals-archive-deal-link image-link" href="<?= $link; ?>"><?= $featuredImage; ?></a>
            <?php if($taxonomies): ?>
                <div class="deals-archive-deal-categories">
                    <?php foreach ($taxonomies as $taxonomy): ?>
                        <?php
                        $taxPrefix = $taxonomy->taxonomy;
                        $taxSlug = $taxonomy->slug;
                        $taxUrl = "/" . $taxPrefix . "/" . $taxSlug;
                        $taxName = $taxonomy->name;
                        ?>
                        <div class="deals-archive-deal-categories-category"><?= $taxName; ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <div class="deals-archive-deal-content">
                <a class="deals-archive-deal-link" href="<?= $link; ?>"><h2 class="deals-archive-deal-title heading-4"><?= $title; ?></h2></a>
                <div class="deals-archive-deal-excerpt paragraph"><?= $excerpt; ?></div>
            </div>
        </div>
	</div><!-- .entry-content -->
</article><!-- #post-${ID} -->
