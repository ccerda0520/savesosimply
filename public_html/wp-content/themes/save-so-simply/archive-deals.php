<?php

get_header();
?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php if ( have_posts() ) : ?>
            <div class='deals-archive' id='deals-archive'>
                <?php
                // Start the Loop.
                while ( have_posts() ) :
                    the_post();

                    $dealClasses = '';
                    $title = get_the_title();
                    $link = get_the_permalink();
                    $featuredImage = get_the_post_thumbnail(get_the_ID(), 'archive-deal-image');
                    $content = get_the_content();
                    $excerpt = explode(" ", $content);
                    $contentLength = count($excerpt);
                    $excerpt = implode(" ", array_splice($excerpt, 0, 65));
                    $excerpt = $contentLength > 65 ? $excerpt . '...' . " <a href='$link'>Read More</a>" : $excerpt;
                    $excerpt = strip_tags($excerpt, '<p><a>');
                    $taxonomies = wp_get_object_terms( get_the_ID(), 'deals_category' );
                    $dealStartDate = get_post_meta(get_the_ID(), 'deal_start_date')[0];
                    $dealStartDate = $dealStartDate !== null ? \DateTime::createFromFormat('Ymd', $dealStartDate) : null;
                    $dealEndDate = get_post_meta(get_the_ID(), 'deal_end_date')[0];
                    $dealEndDate = $dealEndDate !== null ? \DateTime::createFromFormat('Ymd', $dealEndDate) : null;
                    $currentDate = \DateTime::createFromFormat('Ymd', current_time('Ymd'));
                    $expired = $dealEndDate ? $currentDate > $dealEndDate : false;
                    $dealClasses .= $expired ? ' expired' : '';
                    ?>
                    <div class="deals-archive-deal <?= $dealClasses; ?>">
                        <a class="deals-archive-deal-link image-link" href="<?= $link; ?>"><?= $featuredImage; ?></a>
                        <?php if($taxonomies): ?>
                            <div class="deals-archive-deal-categories">
                                <?php foreach ($taxonomies as $taxonomy): ?>
                                    <?php
                                    $taxPrefix = $taxonomy->taxonomy;
                                    $taxSlug = $taxonomy->slug;
                                    $taxUrl = "/" . $taxPrefix . "/" . $taxSlug;
                                    $taxName = $taxonomy->name;
                                    ?>
                                    <div class="deals-archive-deal-categories-category"><?= $taxName; ?></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <div class="deals-archive-deal-content">
                            <a class="deals-archive-deal-link" href="<?= $link; ?>"><h2 class="deals-archive-deal-title heading-4"><?= $title; ?></h2></a>
                            <?php if($dealStartDate || $dealEndDate): ?>
                                <div class="deals-archive-deal-dates">
                                    <?php if($dealStartDate): ?>
                                        <div class="deal-start-date">Start: <span class="date"><?= $dealStartDate->format('m-d-Y'); ?></span></div>
                                    <?php endif; ?>
                                    <?php if($dealEndDate): ?>
                                        <div class="deal-end-date">End: <span class="date"><?= $dealEndDate->format('m-d-Y'); ?></span></div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="deals-archive-deal-excerpt paragraph"><?= $excerpt; ?></div>
                        </div>
                    </div>
                    <?php
                endwhile;
            echo "</div>";
                // Previous/next page navigation.
                twentynineteen_the_posts_navigation();

            // If no content, include the "No posts found" template.
            else :
                get_template_part( 'template-parts/content/content', 'none' );

            endif;
            ?>
        </main><!-- #main -->
        <div id="sidebar">
            <div class="sidebar-content">
                <?php dynamic_sidebar( 'main-side-bar' ); ?>
            </div>
        </div>
    </section><!-- #primary -->

<?php
get_footer();