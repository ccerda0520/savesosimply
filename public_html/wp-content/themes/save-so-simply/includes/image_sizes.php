<?php
add_image_size( 'archive-deal-image', 735, 340, true );

add_action('after_setup_theme', 'sss_update_logo_size', 100);
function sss_update_logo_size()
{
    remove_theme_support('custom-logo');
    add_theme_support(
        'custom-logo',
        array(
            'height'      => 75,
            'width'       => 145,
            'flex-width'  => false,
            'flex-height' => false,
        )
    );

}