<?php
function twentynineteen_post_thumbnail() {
    if ( ! twentynineteen_can_show_post_thumbnail() ) {
        return;
    }

    if ( is_singular() ) :
        ?>

        <div class="post-thumbnail">
            <?php the_post_thumbnail(); ?>
        </div><!-- .post-thumbnail -->

    <?php
    else :
        ?>

        <figure class="post-thumbnail">
            <a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
                <?php the_post_thumbnail( 'post-thumbnail' ); ?>
            </a>
        </figure>

    <?php
    endif; // End is_singular().
}