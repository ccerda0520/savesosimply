<?php

function sss_custom_sidebars() {
    register_sidebar(
        array (
            'name' => __( 'Main Sidebar', 'your-theme-domain' ),
            'id' => 'main-side-bar',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'sss_custom_sidebars' );