<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 12/31/2018
 * Time: 1:24 PM
 */

class DealsPostType
{
    public function __construct()
    {
        add_action('init', [$this, 'registerPostType'], 0);
        add_action('init', [$this, 'registerTaxonomies'], 0);
    }

    public function registerPostType()
    {
// Set UI labels for Custom Post Type
        $labels = array(
            'name' => _x('Deals', 'Post Type General Name', 'twentythirteen'),
            'singular_name' => _x('Deal', 'Post Type Singular Name', 'twentythirteen'),
            'menu_name' => __('Deals', 'twentythirteen'),
            'parent_item_colon' => __('Parent Deal', 'twentythirteen'),
            'all_items' => __('All Deals', 'twentythirteen'),
            'view_item' => __('View Deal', 'twentythirteen'),
            'add_new_item' => __('Add New Deal', 'twentythirteen'),
            'add_new' => __('Add New', 'twentythirteen'),
            'edit_item' => __('Edit Deal', 'twentythirteen'),
            'update_item' => __('Update Deal', 'twentythirteen'),
            'search_items' => __('Search Deal', 'twentythirteen'),
            'not_found' => __('Not Found', 'twentythirteen'),
            'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
        );

// Set other options for Custom Post Type

        $args = array(
            'label' => __('deals', 'twentythirteen'),
            'description' => __('Deal news and reviews', 'twentythirteen'),
            'labels' => $labels,
            // Features this CPT supports in Post Editor
            'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
            // You can associate this CPT with a taxonomy or custom taxonomy.
            'taxonomies' => array('genres'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );

        // Registering your Custom Post Type
        register_post_type('deals', $args);

    }

    public function registerTaxonomies()
    {
        $labels = array(
            'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
            'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
            'menu_name'                  => __( 'Categories', 'text_domain' ),
            'all_items'                  => __( 'All Categories', 'text_domain' ),
            'parent_item'                => __( 'Parent Category', 'text_domain' ),
            'parent_item_colon'          => __( 'Parent Category:', 'text_domain' ),
            'new_item_name'              => __( 'New Category Name', 'text_domain' ),
            'add_new_item'               => __( 'Add New Category', 'text_domain' ),
            'edit_item'                  => __( 'Edit Category', 'text_domain' ),
            'update_item'                => __( 'Update Category', 'text_domain' ),
            'view_item'                  => __( 'View Category', 'text_domain' ),
            'separate_items_with_commas' => __( 'Separate Categories with commas', 'text_domain' ),
            'add_or_remove_items'        => __( 'Add or remove Categories', 'text_domain' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
            'popular_items'              => __( 'Popular Categories', 'text_domain' ),
            'search_items'               => __( 'Search Categories', 'text_domain' ),
            'not_found'                  => __( 'Not Found', 'text_domain' ),
            'no_terms'                   => __( 'No Categories', 'text_domain' ),
            'items_list'                 => __( 'Categories list', 'text_domain' ),
            'items_list_navigation'      => __( 'Categories list navigation', 'text_domain' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );
        register_taxonomy( 'deals_category', array( 'deals' ), $args );
    }
}

$dealsPostType = new DealsPostType();


