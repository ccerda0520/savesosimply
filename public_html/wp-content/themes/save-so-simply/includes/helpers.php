<?php
// allow shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );