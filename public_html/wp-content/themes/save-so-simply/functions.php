<?php
function load_theme_scripts() {
    wp_enqueue_style('sss-style', get_stylesheet_directory_uri() . '/assets/css/app.css');
    wp_enqueue_script('sss-script', get_stylesheet_directory_uri() . '/assets/js/app.js');
    wp_dequeue_script('twentynineteen-touch-navigation');
    wp_dequeue_script('twentynineteen-priority-menu');
}

add_action('wp_enqueue_scripts', 'load_theme_scripts', 1000);


function disable_gutenberg_on_post($is_enabled, $post_type) {

    if ($post_type === 'post') return false; // change book to your post type

    return $is_enabled;

}
add_filter('use_block_editor_for_post_type', 'disable_gutenberg_on_post', 10, 2);

// Include all files in the includes directory
foreach (glob(get_stylesheet_directory() . '/includes/*.php') as $filename) {
    /** @noinspection PhpIncludeInspection */
    require($filename);
}

// add gutenberg block editor specific styles
function kern_enqueue_editor_scripts() {

    wp_enqueue_style( 'kern-editor-customizer-styles', get_stylesheet_directory_uri() . '/assets/css/editor.css', ['twentynineteen-editor-customizer-styles'], '1.1', 'all' );

}
add_action( 'enqueue_block_editor_assets', 'kern_enqueue_editor_scripts' );

// add options page
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}
