<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MC7X5DX');</script>
    <!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MC7X5DX"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentynineteen' ); ?></a>
		<header id="masthead" class="<?php echo is_singular() && twentynineteen_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">
            <?php
            $socialMediaAccounts = get_option('wpseo_social');
            $headerOptions = function_exists('get_field') ? get_field('header', 'options') : '';
            $socialHeaderText = $headerOptions['social_header_text'];
            echo "<div class='social-top-bar'><div class='social-container'>";
            if($socialHeaderText) {
               echo "<div class='social-header-text'>$socialHeaderText</div>";
            }
            if(!empty($socialMediaAccounts)) {
                echo "<div class='social-icons'>";
                foreach ($socialMediaAccounts as $key => $socialMediaAccount) {
                    if($key === 'facebook_site' && $socialMediaAccount) {
                        echo "<a class='sss-icon-facebook' href='$socialMediaAccount' target='_blank'><span class='screen-reader-text'>Facebook Link</span></a>";
                    }
                    if($key === 'instagram_url' && $socialMediaAccount) {
                        echo "<a class='sss-icon-instagram' href='$socialMediaAccount' target='_blank'><span class='screen-reader-text'>Instagram Link</span></a>";
                    }
                    if($key === 'twitter_site' && $socialMediaAccount) {
                        echo "<a class='sss-icon-twitter' href='https://twitter.com/$socialMediaAccount' target='_blank'><span class='screen-reader-text'>Twitter Link</span></a>";
                    }
                    if($key === 'pinterest_url' && $socialMediaAccount) {
                        echo "<a class='sss-icon-pinterest' href='$socialMediaAccount' target='_blank'><span class='screen-reader-text'>Pinterest Link</span></a>";
                    }
                    if($key === 'youtube_url' && $socialMediaAccount) {
                        echo "<a class='sss-icon-youtube' href='$socialMediaAccount' target='_blank'><span class='screen-reader-text'>Youtube Link</span></a>";
                    }
                }
                echo "</div>";
            }
            echo "</div></div>";
            ?>
			<div class="site-branding-container">
				<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div><!-- .layout-wrap -->
            <div class="header-menu">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_class'     => 'main-menu',
                        'items_wrap'     => '<ul id="%1$s" class="%2$s" tabindex="0">%3$s</ul>',
                    )
                );
                ?>
            </div>
		</header><!-- #masthead -->
	<div id="content" class="site-content">
