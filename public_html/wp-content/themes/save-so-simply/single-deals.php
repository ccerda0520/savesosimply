<?php

get_header();
?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="main-content">
                <?php if ( is_singular() ) : ?>
                    <div class="site-featured-image">
                        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                        }
                        ?>
                        <?php
                        $taxonomies = wp_get_object_terms( get_the_ID(), 'deals_category' );
                        $dealStartDate = get_post_meta(get_the_ID(), 'deal_start_date')[0];
                        $dealStartDate = $dealStartDate !== null ? \DateTime::createFromFormat('Ymd', $dealStartDate) : null;
                        $dealEndDate = get_post_meta(get_the_ID(), 'deal_end_date')[0];
                        $dealEndDate = $dealEndDate !== null ? \DateTime::createFromFormat('Ymd', $dealEndDate) : null;
                        $currentDate = \DateTime::createFromFormat('Ymd', current_time('Ymd'));
                        $expired = $dealEndDate ? $currentDate > $dealEndDate : false;
                        $dealClasses .= $expired ? ' expired' : '';
                        twentynineteen_post_thumbnail();
                        if($taxonomies): ?>
                            <div class="deal-categories">
                                <?php foreach ($taxonomies as $taxonomy): ?>
                                    <?php
                                    $taxPrefix = $taxonomy->taxonomy;
                                    $taxSlug = $taxonomy->slug;
                                    $taxUrl = "/" . $taxPrefix . "/" . $taxSlug;
                                    $taxName = $taxonomy->name;
                                    ?>
                                    <div class="deal-category"><?= $taxName; ?></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif;
                        the_post();
                        $discussion = ! is_page() && twentynineteen_can_show_post_thumbnail() ? twentynineteen_get_discussion_data() : null;

                        $classes = 'entry-header';
                        if ( ! empty( $discussion ) && absint( $discussion->responses ) > 0 ) {
                            $classes = 'entry-header has-discussion';
                        }
                        ?>
                        <div class="<?php echo $classes; ?>">
                            <?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
                        </div><!-- .entry-header -->
                        <?php
                        if($dealStartDate || $dealEndDate): ?>
                            <div class="deal-dates">
                                <?php if($dealStartDate): ?>
                                    <div class="deal-start-date">Start: <span class="date"><?= $dealStartDate->format('m-d-Y'); ?></span></div>
                                <?php endif; ?>
                                <?php if($dealEndDate): ?>
                                    <div class="deal-end-date">End: <span class="date"><?= $dealEndDate->format('m-d-Y'); ?></span></div>
                                <?php endif; ?>
                            </div>
                        <?php endif;
                        if($expired) :
                            echo "<h6 class='deal-expired'>Expired</h6>";
                        endif;
                        ?>
                        <?php rewind_posts(); ?>
                    </div>
                <?php endif; ?>
                <?php

                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();

                    the_content();

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }

                endwhile; // End of the loop.
                ?>

            </div>
        </main><!-- #main -->
        <div id="sidebar">
            <div class="sidebar-content">
                <?php dynamic_sidebar( 'main-side-bar' ); ?>
            </div>
        </div>
    </section><!-- #primary -->

<?php
get_footer();