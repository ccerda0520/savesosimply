<?php
/**
 * Plugin's bootstrap file to launch the plugin.
 *
 *
 * @wordpress-plugin
 * Plugin Name: CBC Blocks
 * Plugin URI:  https://componentbasedcarlos.com
 * Description: Collection of CBC Blocks
 * Version:     1.0.0
 * Author:      Component Based Carlos
 * Author URI:  https://componentbasedcarlos.com
 * Text Domain: brandastic_blocks
 * Domain Path: /languages
 * License:     GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

namespace CBC\Blocks;

//  Exit if accessed directly.
defined('ABSPATH') || exit;

/**
 * Gets this plugin's absolute directory path.
 *
 * @since  2.1.0
 * @ignore
 * @access private
 *
 * @return string
 */
function _get_plugin_directory() {
	return __DIR__;
}

/**
 * Gets this plugin's URL.
 *
 * @since  2.1.0
 * @ignore
 * @access private
 *
 * @return string
 */
function _get_plugin_url() {
	static $plugin_url;

	if ( empty( $plugin_url ) ) {
		$plugin_url = plugins_url( null, __FILE__ );
	}

	return $plugin_url;
}

// Enqueue JS and CSS
include __DIR__ . '/lib/enqueue-scripts.php';

// Include all index.php files in the blocks directory, used for rendering via PHP instead of JS
foreach (glob(_get_plugin_directory() . '/blocks/*/index.php') as $filename)
{
    /** @noinspection PhpIncludeInspection */
    require($filename);
}