<?php

namespace CBC\Blocks;

add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\enqueue_block_editor_assets' );
/**
 * Enqueue block editor only JavaScript and CSS.
 */
function enqueue_block_editor_assets() {
	// Make paths variables so we don't write em twice ;)
	$block_path = '/assets/js/editor.blocks.js';
	$style_path = '/assets/css/blocks.editor.css';

	// Enqueue the bundled block JS file
	wp_enqueue_script(
		'brandastic-blocks-js',
		_get_plugin_url() . $block_path,
		[ 'wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor' ],
		filemtime( _get_plugin_directory() . $block_path )
	);

//	// Enqueue optional editor only styles
//	wp_enqueue_style(
//		'brandastic-blocks-editor-css',
//		_get_plugin_url() . $style_path,
//		[ ],
//		filemtime( _get_plugin_directory() . $style_path )
//	);
}

add_action( 'enqueue_block_assets', __NAMESPACE__ . '\enqueue_assets' );
/**
 * Enqueue front end and editor JavaScript and CSS assets.
 */
function enqueue_assets() {
	$style_path = '/assets/css/blocks.style.css';
	wp_enqueue_style(
		'brandastic-blocks',
		_get_plugin_url() . $style_path,
		null,
		filemtime( _get_plugin_directory() . $style_path )
	);
}

add_action( "wp_enqueue_scripts", __NAMESPACE__ . '\frontend_assets' );
/**
 * Enqueue block frontend JavaScript
 */
function frontend_assets(){

    $frontend_js_path = "/assets/js/blocks.frontend.js";

    wp_enqueue_script(
        "cbc-blocks-frontend-js",
        _get_plugin_url() . $frontend_js_path,
        ['wp-element'],
        filemtime( _get_plugin_directory() . $frontend_js_path ),
        true
    );
}