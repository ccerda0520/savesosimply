/**
 * Block dependencies
 */


/**
 * Block libraries
 */

import style from './style.scss';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const {
    BlockControls,
    InspectorControls,
    MediaUpload,
    MediaPlaceholder
} = wp.editor;
const {
    IconButton,
    Toolbar,
    PanelBody,
    PanelRow,
    RangeControl
} = wp.components;

/**
 * Register block
 */

export default registerBlockType("cbc-blocks/deals-archive", {
    title: __("Deals Archive", "cbc-blocks"),
    description: __("Deals Archive", "cbc-blocks"),
    category: "widgets",
    attributes: {
        postsPerPage: {
            type: "number",
                default: 10
        },
    },
    supports: {
        align: ["full", "wide"],
        alignWide: true
    },
    edit: props => {
        const {
            attributes: { postsPerPage },
            className,
            setAttributes
        } = props;

        return (
            <Fragment>
                <InspectorControls>
                    <PanelBody
                        title={__("Archive Settings", "cbc-blocks")}
                        initialOpen={true}
                    >
                        <PanelRow>
                            <h3>Desktop Count</h3>
                            <RangeControl
                                beforeIcon="arrow-left-alt2"
                                afterIcon="arrow-right-alt2"
                                value={postsPerPage}
                                onChange={postsPerPage => setAttributes({ postsPerPage })}
                                min={5}
                                max={25}
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>

                <div className={`${className}`}>
                    Deals Archive
                </div>
            </Fragment>
        );
    },
    save: props => {
        return null;
    }
});
