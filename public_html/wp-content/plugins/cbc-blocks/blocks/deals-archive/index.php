<?php
namespace CBC\Blocks\Deals_archive;


use Faker\Provider\DateTime;

add_action( 'plugins_loaded', __NAMESPACE__ . '\register_dynamic_block' );
/**
 * Register the dynamic block.
 *
 * @since 2.1.0
 *
 * @return void
 */
function register_dynamic_block() {

    // Only load if Gutenberg is available.
    if ( ! function_exists( 'register_block_type' ) ) {
        return;
    }

    // Hook server side rendering into render callback
    register_block_type( 'cbc-blocks/deals-archive', [
        'render_callback' => __NAMESPACE__ . '\render_deals_archive_block',
    ] );

}

/**
 * Server rendering for /blocks/examples/12-dynamic
 */
function render_deals_archive_block($attributes) {
    $html = '';
    $postsPerPage = $attributes['postsPerPage'];
    $pageNumber = sanitize_text_field($_GET['page_num']) ? (int) sanitize_text_field($_GET['page_num']) : 1;
    $queryArgs = [
        'post_type' => 'deals',
        'post_status' => 'publish',
        'paged' => $pageNumber,
        'posts_per_page' => $postsPerPage
    ];
    $dealsQuery = new \WP_Query($queryArgs);
    $pages = $dealsQuery->max_num_pages;
    $paginationHtml = '';
    if($pages && $pages != 1) {
        $paginationHtml .= "<div class='deals-archive-pagination' id='deals-archive-pagination'>";
        for($i=0;$i<$pages;$i++) {
            $currentPageNumber = $i + 1;
            if($pageNumber == $currentPageNumber) {
                $paginationHtml .= "<div class='pagination-link current-page'>$currentPageNumber</div>";
                continue;
            }
            $redirectUrl = $_SERVER['REDIRECT_URL'];
            $queryArray = [];
            parse_str($_SERVER['REDIRECT_QUERY_STRING'],$queryArray);
            $queryArray['page_num'] = $currentPageNumber;
            $queryEncoded = http_build_query($queryArray);
            $pageUrl = $redirectUrl . '?' . $queryEncoded;
            $paginationHtml .= "<div class='pagination-link'><a href='$pageUrl'>$currentPageNumber</a></div>";
        }
        $paginationHtml .= "</div>";
    }
    $html = '';

    if($dealsQuery->have_posts()) :
        $html .= "<div class='deals-archive' id='deals-archive'>";
    while($dealsQuery->have_posts()) : $dealsQuery->the_post();
        $dealClasses = '';
        $title = get_the_title();
        $link = get_the_permalink();
        $featuredImage = get_the_post_thumbnail(get_the_ID(), 'archive-deal-image');
        $content = get_the_content();
        $excerpt = explode(" ", $content);
        $contentLength = count($excerpt);
        $excerpt = implode(" ", array_splice($excerpt, 0, 65));
        $excerpt = $contentLength > 65 ? $excerpt . '...' . " <a href='$link'>Read More</a>" : $excerpt;
        $excerpt = strip_tags($excerpt, '<p><a>');
        $taxonomies = wp_get_object_terms( get_the_ID(), 'deals_category' );
        $dealStartDate = get_post_meta(get_the_ID(), 'deal_start_date')[0];
        $dealStartDate = $dealStartDate !== null ? \DateTime::createFromFormat('Ymd', $dealStartDate) : null;
        $dealEndDate = get_post_meta(get_the_ID(), 'deal_end_date')[0];
        $dealEndDate = $dealEndDate !== null ? \DateTime::createFromFormat('Ymd', $dealEndDate) : null;
        $currentDate = \DateTime::createFromFormat('Ymd', current_time('Ymd'));
        $expired = $dealEndDate ? $currentDate > $dealEndDate : false;
        $dealClasses .= $expired ? ' expired' : '';
        ob_start()
        ?>
    <div class="deals-archive-deal <?= $dealClasses; ?>">
        <a class="deals-archive-deal-link image-link" href="<?= $link; ?>"><?= $featuredImage; ?></a>
        <?php if($taxonomies): ?>
        <div class="deals-archive-deal-categories">
            <?php foreach ($taxonomies as $taxonomy): ?>
            <?php
                $taxPrefix = $taxonomy->taxonomy;
                $taxSlug = $taxonomy->slug;
                $taxUrl = "/" . $taxPrefix . "/" . $taxSlug;
                $taxName = $taxonomy->name;
            ?>
            <div class="deals-archive-deal-categories-category"><?= $taxName; ?></div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <div class="deals-archive-deal-content">
            <a class="deals-archive-deal-link" href="<?= $link; ?>"><h2 class="deals-archive-deal-title heading-4"><?= $title; ?></h2></a>
            <?php if($dealStartDate || $dealEndDate): ?>
                <div class="deals-archive-deal-dates">
                    <?php if($dealStartDate): ?>
                        <div class="deal-start-date">Start: <span class="date"><?= $dealStartDate->format('m-d-Y'); ?></span></div>
                    <?php endif; ?>
                    <?php if($dealEndDate): ?>
                        <div class="deal-end-date">End: <span class="date"><?= $dealEndDate->format('m-d-Y'); ?></span></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div class="deals-archive-deal-excerpt paragraph"><?= $excerpt; ?></div>
        </div>
    </div>
    <?php
    $html .= ob_get_clean();
    endwhile;
        $html .= "</div>";
        wp_reset_postdata();
    endif;
    $html .= $paginationHtml;
    return $html;
}