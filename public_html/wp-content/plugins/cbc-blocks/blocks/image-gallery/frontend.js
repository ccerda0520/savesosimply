const { render } = wp.element;

import Gallery from './gallery';

import {getArrayGroups} from './helpers';

const galleries = document.querySelectorAll(
    ".wp-block-cbc-blocks-image-gallery"
);

galleries.forEach((gallery, index) => {
    const imageCount = gallery.dataset.image_count;
    const images = gallery.querySelectorAll("img");
    let photos = [];
    images.forEach(image => {
        photos.push({
            src: image.src,
            width: image.width,
            height: image.height,
            alt: image.alt,
            caption: image.title
        });
    });
    const imageRows = getArrayGroups(photos, imageCount);
    console.log(imageRows);
    render(
        <Gallery
            imageRows={imageRows}
        />,
        gallery
    );
});