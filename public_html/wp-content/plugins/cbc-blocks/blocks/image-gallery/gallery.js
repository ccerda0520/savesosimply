export default class Gallery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeRow: 0
        };
    }
    componentDidMount() {
        console.log(this.props.imageRows.length);
        setInterval(() => {
            const rowCount = this.props.imageRows.length;
            let nextActiveRow = this.state.activeRow < (rowCount - 1) ? this.state.activeRow + 1 : 0;
            this.setState({
                activeRow: nextActiveRow
            });
            console.log(nextActiveRow);
        }, 5000);
    }

    render() {
        const {
            imageRows
        } = this.props;
        const {
            activeRow
        } = this.state;
        return (
            <div className="image-gallery">
                {
                    imageRows.map((imageRow, index) => {
                        return (
                            <div
                                className={`image-row image-row-${index} ${index === activeRow ? 'active' : ''}`}
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}
                            >
                                {
                                    imageRow.map(img => {
                                        return (
                                            <div
                                                style={{
                                                    padding: "0 25px",
                                                    textAlign: "center"
                                                }}
                                            >
                                                <img
                                                    src={img.src}
                                                    alt={img.alt}
                                                    title={img.caption}
                                                    width={img.width}
                                                    height="auto"
                                                    style={{
                                                        width: img.width,
                                                        height: "auto"
                                                    }}
                                                />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}