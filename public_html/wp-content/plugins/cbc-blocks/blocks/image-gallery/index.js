/**
 * Block dependencies
 */


/**
 * Block libraries
 */

import Gallery from './gallery';
import {getArrayGroups} from './helpers';
import style from './style.scss';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const {
    BlockControls,
    InspectorControls,
    MediaUpload,
    MediaPlaceholder
} = wp.editor;
const {
    IconButton,
    Toolbar,
    PanelBody,
    PanelRow,
    RangeControl
} = wp.components;

/**
 * Register block
 */

export default registerBlockType("cbc-blocks/image-gallery", {
    title: __("Image Gallery", "cbc-blocks"),
    description: __("Image Gallery", "cbc-blocks"),
    category: "widgets",
    attributes: {
        images: {
            type: "array",
            default: []
        },
        desktopImageCount: {
            type: "number",
            default: 4
        },
        tabletImageCount: {
            type: "number",
            default: 3
        },
        mobileImageCount: {
            type: "number",
            default: 2
        }
    },
    supports: {
        align: ["full", "wide"],
        alignWide: true
    },
    edit: props => {
        const {
            attributes: { images, desktopImageCount },
            className,
            setAttributes
        } = props;

        const onSelectImages = newImages => {
            const images = newImages.map(img =>
                Object.assign(
                    {},
                    {
                        src: img.sizes.full.url,
                        width: img.sizes.full.width,
                        height: img.sizes.full.height,
                        id: img.id,
                        alt: img.alt,
                        caption: img.caption
                    }
                )
            );
            setAttributes({ images });
        };

        const windowWidth = document.documentElement.clientWidth;


        return (
            <Fragment>
                <InspectorControls>
                    <PanelBody
                        title={__("Gallery Settings", "cbc-blocks")}
                        initialOpen={true}
                    >
                        <PanelRow>
                            <h3>Desktop Count</h3>
                            <RangeControl
                                beforeIcon="arrow-left-alt2"
                                afterIcon="arrow-right-alt2"
                                value={desktopImageCount}
                                onChange={desktopImageCount => setAttributes({ desktopImageCount })}
                                min={1}
                                max={10}
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                {!!images.length && (
                    <BlockControls>
                        <Toolbar>
                            <MediaUpload
                                allowedTypes={["images"]}
                                multiple
                                gallery
                                value={images.map(img => img.id)}
                                onSelect={onSelectImages}
                                render={({ open }) => (
                                    <IconButton
                                        className="components-toolbar__control"
                                        label={__("Edit Gallery", "jsforwpadvblocks")}
                                        icon="edit"
                                        onClick={open}
                                    />
                                )}
                            />
                        </Toolbar>
                    </BlockControls>
                )}
                <div className={`${className}`}>
                    {!!!images.length ? (
                        <MediaPlaceholder
                            labels={{
                                title: __("Gallery", "jsforwpadvblocks"),
                                instructions: __(
                                    "Drag images, upload new ones or select files from your library",
                                    "jsforwpadvblocks"
                                )
                            }}
                            accept="images/*"
                            multiple
                            onSelect={onSelectImages}
                        />
                    ) : (
                        <div
                            style={{
                                display: "flex"
                            }}
                        >
                            {images.map(img => (
                                <div
                                    style={!!desktopImageCount ? {
                                        width: 100/desktopImageCount + "%",
                                        textAlign: "center"
                                    } : null}
                                >
                                    <img
                                        src={img.src}
                                        alt={img.alt}
                                        title={img.caption}
                                        width={img.width}
                                        height="auto"
                                        data-id={img.id}
                                        style={{
                                            width: img.width,
                                            height: "auto"
                                        }}
                                    />
                                </div>
                            ))}
                        </div>

                    )}
                </div>
            </Fragment>
        );
    },
    save: props => {
        const { images, desktopImageCount } = props.attributes;
        const { className } = props;
        const windowWidth = document.documentElement.clientWidth;
        let imageRows = getArrayGroups(images, desktopImageCount);
        return (
            <div
                className={`${className} image-rows`}
                data-image_count={desktopImageCount}
            >
                <Gallery
                    imageRows={imageRows}
                />
            </div>
        );
    }
});
