/**
 * Internal block libraries
 */
const {__} = wp.i18n;
const {Component} = wp.element;
const {
    InspectorControls
} = wp.editor;

const {
    PanelBody,
    PanelRow,
    RangeControl,
} = wp.components;

/**
 * Create an Inspector Controls wrapper Component
 */
export default class Inspector extends Component {
    constructor() {
        super(...arguments);
    }

    render() {
        const {
            attributes: {
                desktopImageCount,
                tabletImageCount,
                mobileImageCount
            },
            setAttributes
        } = this.props;

        return (
            <InspectorControls>
                <PanelBody
                    initialOpen={false}>
                </PanelBody>
                <PanelBody>
                    <RangeControl
                        beforeIcon="arrow-left-alt2"
                        afterIcon="arrow-right-alt2"
                        label={__("", "jsforwpblocks")}
                        value={desktopImageCount}
                        onChange={desktopImageCount => setAttributes({desktopImageCount})}
                        min={1}
                        max={10}
                    />
                </PanelBody>
            </InspectorControls>
        );
    }
}
