export const getArrayGroups = (array,groupSize) => {
    let groupArray = [];
    for (var i=0; i <array.length; i+=groupSize) {
        groupArray.push(array.slice(i,i+groupSize));
    }
    return groupArray;
};